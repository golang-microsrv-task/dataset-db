FROM postgres:12.4-alpine
COPY ./docker-entrypoint-initdb.d/ /docker-entrypoint-initdb.d/
ENV POSTGRES_USER=postgres POSTGRES_PASSWORD=password POSTGRES_DB=postgres